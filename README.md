# gal - Graphical Application Launcher(1)
Version 0.02 *2021/07/11*

```
$ gal [options]
```

# DESCRIPTION
**gal** is an application launcher for the graphical environment. When the user invokes **gal** it will search for all executable files within **PATH** and then prompt the user for which program they would like to execute using **PROMPT_MENU**. If the returned string from **PROMPT_MENU** is prepended with a *'>'*, then **gal** will execute said program within a **TERMINAL** and then spawn an interactive **SHELL**; otherwise, **gal** will execute the provided program in the background. Any arguments provided to **gal** will be prepended onto the **PROMPT_MENU** before any *'-'* arguments.

# ENVIRONMENT
* **PATH**

A colon separated list of directories containing programs.

* **PROMPT_MENU**

A program that accepts newline lists from stdin and can take a prompt string (example: dmenu(1)).

* **TERMINAL**

A terminal emulator.

* **SHELL**

A POSIX-compliant shell.

# EXAMPLES
* Run **gal** without passing any extra arguments to **PROMPT_MENU**.
```
$ gal
```
* Run **gal** and pass extra arguments to **PROMPT_MENU**.
```
$ gal -foo -bar
```
* Run **gal** and pass extra arguments and their parameters to **PROMPT_MENU**.
```
$ gal -foo "bar" -buzz "bax"
```

# EXIT STATUS
* **0**

Successfully completed the specified task.

* **>0**

An error occurred.

# SEE ALSO
* dmenu(1)
* rofi(1)
* st(1)
* xterm(1)
* sh(1)
